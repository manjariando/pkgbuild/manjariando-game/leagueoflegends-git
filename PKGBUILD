# Maintainer: Kuan-Yen Chou <kuanyenchou@gmail.com>
# Contributor: Timofey Titovets <nefelim4ag@gmail.com>

pkgname=leagueoflegends-git
pkgver=0.11.22.r0
pkgrel=1
pkgdesc="League of Legends helper script"
arch=('any')
url="https://github.com/kyechou/leagueoflegends"
license=('GPL3')
makedepends=('git' 'imagemagick')
conflicts=("${pkgname%%-git}")
install=${pkgname%%-git}.install
source=("$pkgname"::'git+https://github.com/kyechou/leagueoflegends.git'
        "https://metainfo.manjariando.com.br/${pkgname%%-git}/com.${pkgname%%-git}.metainfo.xml")
md5sums=('SKIP'
         'e61dd01422939de8c63057960736eddd')

pkgver() {
    cd "$srcdir/$pkgname"
    _pkgver=$(git describe --long --tags | sed 's/^v//;s/\([^-]*-g\)/r\1/;s/-/./g')
    printf '%s' "$( cut -f1-4 -d'.' <<< ${_pkgver} )"
}

package() {
    depends=('wine-lol>=5.18' 'winetricks-git' 'bash' 'curl' 'openssl'
            'lib32-gnutls' 'lib32-libldap' 'lib32-openal' 'lib32-libpulse'
            'vulkan-icd-loader' 'lib32-vulkan-icd-loader' 'vulkan-driver'
            'lib32-vulkan-driver')
    optdepends=("lib32-amdvlk: AMD Vulkan driver"
                "lib32-nvidia-utils: NVIDIA Vulkan driver"
                "lib32-vulkan-intel: Intel's Vulkan mesa driver"
                "lib32-vulkan-radeon: Radeon's Vulkan mesa driver"
                "zenity: Loading screen indication")

    cd "$srcdir/$pkgname"
    make DESTDIR="${pkgdir}" install

    # Appstream
    install -Dm644 "${srcdir}/com.${pkgname%%-git}.metainfo.xml" "${pkgdir}/usr/share/metainfo/com.${pkgname%%-git}.metainfo.xml"
    mv "${pkgdir}/usr/share/applications/${pkgname%%-git}.desktop" "${pkgdir}/usr/share/applications/com.${pkgname%%-git}.desktop"

    for size in 22 24 32 48 64 128; do
        mkdir -p "${pkgdir}/usr/share/icons/hicolor/${size}x${size}/apps"
        convert "${pkgdir}/usr/share/icons/hicolor/256x256/apps/${pkgname%-git}.png" -resize "${size}x${size}" \
            "${pkgdir}/usr/share/icons/hicolor/${size}x${size}/apps/${pkgname%-git}.png"
    done
}
